package com.wposs.inventariowposs

import android.content.Intent
import android.os.Bundle
import android.provider.AlarmClock.EXTRA_MESSAGE
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.QuerySnapshot


class MainActivity : AppCompatActivity() {

    val rootRef = FirebaseFirestore.getInstance()
    var opcion : String = ""
    val subjects: MutableList<String?> = ArrayList()
    private lateinit var etnombre : EditText
    private lateinit var btguardar : Button
    private lateinit var etdocumento : EditText
    private lateinit var etpais : EditText
    private lateinit var etciudad : EditText
    private lateinit var etdireccion : EditText
    private lateinit var ettelefono : EditText


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(findViewById(R.id.toolbar))

        etnombre = findViewById<EditText>(R.id.nombre)
        btguardar = findViewById<Button>(R.id.btguardar)
        etdocumento = findViewById<EditText>(R.id.documento)
        etpais = findViewById<EditText>(R.id.pais)
        etciudad = findViewById<EditText>(R.id.ciudad)
        etdireccion = findViewById<EditText>(R.id.direccion)
        ettelefono = findViewById<EditText>(R.id.telefono)
        llenarSpinner()
        btguardar.setOnClickListener{
            validarDatos()
        }
    }

    private fun llenarSpinner() {
        subjects.add("Área a la que pertenece")
        val subjectsRef = rootRef.collection("areas")
        val spinner = findViewById<View>(R.id.listaArea) as Spinner
        val adapter = ArrayAdapter(applicationContext,android.R.layout.simple_spinner_dropdown_item,subjects)
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinner.adapter = adapter
        subjectsRef.get().addOnCompleteListener(OnCompleteListener<QuerySnapshot> { task ->
            if (task.isSuccessful) {
                for (document in task.result!!) {
                    val subject = document.getString("nombreArea")
                    subjects.add(subject)
                }
                adapter.notifyDataSetChanged()
            }
        })
        spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(p0: AdapterView<*>?) {}
             override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                opcion = subjects[p2].toString()
            }
        }
    }

    private fun validarDatos() {
        val nombre = etnombre.text.toString()
        val documento = etdocumento.text.toString()
        val pais = etpais.text.toString()
        val ciudad = etciudad.text.toString()
        val direccion = etdireccion.text.toString()
        val telefono = ettelefono.text.toString()
        if (opcion != "Área a la que pertenece" && !nombre.equals("") && !documento.equals("")
            && !pais.equals("") && !ciudad.equals("") && !direccion.equals("") && !telefono.equals("")){
            val dato = hashMapOf(
                "nombre" to nombre,
                "documento" to documento,
                "pais" to pais,
                "ciudad" to ciudad,
                "area" to opcion,
                "direccion_residencia" to direccion,
                "telefono" to telefono
            )
            rootRef.collection("usuarios")
                .add(dato)
                .addOnSuccessListener {Toast.makeText(this@MainActivity, "Datos almacenados correctamente", Toast.LENGTH_SHORT).show()}
                .addOnFailureListener { e -> Toast.makeText(this@MainActivity,"Error al almacenar datos $e", Toast.LENGTH_SHORT).show()}
            etnombre.setText("")
            etdocumento.setText("")
            etpais.setText("")
            etciudad.setText("")
            etdireccion.setText("")
            ettelefono.setText("")
            subjects.clear()
            llenarSpinner()
            val intent =  Intent ( this ,  ActivityConsultaImplementosUser :: class . java ).apply{
                putExtra("name", nombre)
            }
            startActivity(intent)
        }else{
            Toast.makeText(this@MainActivity, "Complete los campos requeridos", Toast.LENGTH_SHORT).show()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }
}


