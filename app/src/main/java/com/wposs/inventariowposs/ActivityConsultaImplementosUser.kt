package com.wposs.inventariowposs

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.QuerySnapshot
import kotlinx.android.synthetic.main.content_main.*

class ActivityConsultaImplementosUser : AppCompatActivity() {

    private lateinit var recyclerView: RecyclerView
    private lateinit var viewAdapter: RecyclerView.Adapter<*>
    private lateinit var viewManager: RecyclerView.LayoutManager

    val rootRef = FirebaseFirestore.getInstance()
    val subjects2: MutableList<String?> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_consulta_implementos_user)
        val nombre = intent.getStringExtra("name")
        consultaUsuario(nombre)
    }

    private fun consultaUsuario(nombre:String) {
        val subjectsRef = rootRef.collection("usuarios")
            .whereEqualTo("nombre", nombre)
        subjectsRef.get().addOnCompleteListener(OnCompleteListener<QuerySnapshot> { task ->
            if (task.isSuccessful) {
                for (document in task.result!!) {
                    val idUser = document.id
                    consultaImplementos(idUser)
                }
            }
        })
    }

    private fun consultaImplementos(idUser: String) {
        val subjectsRef = rootRef.collection("usuarios").document(idUser)
            .collection("implementos")
        subjectsRef.get().addOnCompleteListener(OnCompleteListener<QuerySnapshot> { task ->
            if (task.isSuccessful) {
                for (document in task.result!!) {
                    val subject = document.getString("nombre")
                    subjects2.add(subject)
                    //txtView.text = subjects2.toString()
                    viewManager = LinearLayoutManager(this)
                    viewAdapter = MyAdapter(subjects2)
                    recyclerView = findViewById<RecyclerView>(R.id.my_recycler_view).apply {
                        // use this setting to improve performance if you know that changes
                        // in content do not change the layout size of the RecyclerView
                        setHasFixedSize(true)
                        // use a linear layout manager
                        layoutManager = viewManager
                        // specify an viewAdapter (see also next example)
                        adapter = viewAdapter
                    }
                }
            }
        })
    }
}